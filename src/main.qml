﻿import QtQuick 2.4
import QtQuick.Controls 1.3

ApplicationWindow
{
   id: appwindow
   property int sekunden: 10
   property bool started: false
   title: qsTr("Kukki-Toaster")
   width: 480
   height: 800
   visible: true
   color: "black"  

   onSekundenChanged:
   {
       return appwindow.sekunden;
   }

   Image
   {
       id: tachoblanck
       width: Math.min(appwindow.width, appwindow.height)-12;
       height: width
       x: appwindow.width/2-width/2
       y: appwindow.height/2-height/2
       source: "/pics/Tacho_Mitte.png"
       scale: 1.1;
   }//Image   

   ViertelKreisLinks
   {
        id: leftelement
        w: (Math.min(appwindow.width, appwindow.height)-12)/2
        h: w
        xpos: appwindow.width/2-w
        ypos: appwindow.height/2
        visible: true

        onLanguageOn:
        {
            leftelement.elementsLeftInvisible();
            rightelement.elementsRightInvisible();
            sprache.show();
        }

        onStartToaster:
        {
            img.visible = true;
            speedometer.visible = true;
            speedometer.startUnfreeze();
        }
   }// ViertelKreisLinks

   ViertelKreisRechts
   {
        id: rightelement
        w: (Math.min(appwindow.width, appwindow.height)-12)/2
        h: w
        xpos: appwindow.width/2
        ypos: appwindow.height/2
        sprache: "/text/Englisch.txt"
        visible: true

        onSettingsOn:
        {
            leftelement.elementsLeftInvisible();
            rightelement.elementsRightInvisible();
            settings.show();
        }

        onInfoOn:
        {
            leftelement.elementsLeftInvisible();
            rightelement.elementsRightInvisible();
            info.readTextFile(rightelement.sprache);
            info.show();
        }        
   }// ViertelKreisRechts

   FlagCircle
   {
        id: kreis_btn
        d: Math.min(appwindow.width, appwindow.height)-4;
        x: appwindow.width/2-d/2
        y: appwindow.height/2-d/2
        image: "/pics/iglo.png"
        skala: 1.1

        onClicked:
        {
            if(kreis_btn.d === Math.min(appwindow.width, appwindow.height)-4)
            {
                großzuklein.start();
            }
            else
            {
                kleinzugroß.start();
            }
        }
   }// FlagCircle

   PropertyAnimation
   {
       id: großzuklein
       target: kreis_btn
       property:"d"
       to: (Math.min(appwindow.width, appwindow.height)-4) * 0.43051
       duration: 500

       onStopped:
       {
           leftelement.elementsLeftVisible();
           rightelement.elementsRightVisible();
       }
   }

   PropertyAnimation
   {
       id: kleinzugroß
       target: kreis_btn
       property: "d"
       to: Math.min(appwindow.width, appwindow.height)-4
       duration: 500

       onStarted:
       {
           leftelement.elementsLeftInvisible();
           rightelement.elementsRightInvisible();
       }
   }

   Sprachen // Greyring mit Flaggen
   {
        id: sprache
        width: Math.min(appwindow.width, appwindow.height)-4;
        height: width
        radius: width/2
        x: appwindow.width/2-width/2
        y: appwindow.height/2-height/2
        language: "/text/Englisch.txt"

        onLanguageOff:
        {
            leftelement.elementsLeftVisible();
            rightelement.elementsRightVisible();
			rightelement.sprache = lng;
        }
   }// Sprachen

   Settings // Heizstufen-Auswahl
   {
        id: settings
        width: Math.min(appwindow.width, appwindow.height)-4;
        height: width
        radius: width/2
        x: appwindow.width/2-width/2
        y: appwindow.height/2-height/2

        onSettingsOff:
        {
            leftelement.elementsLeftVisible();
            rightelement.elementsRightVisible();
        }

        onLevelIsSet:
        {
            if(appwindow.started == false)
            {
                appwindow.sekunden = umleiten;
                appwindow.started = true;
            }
            else
            {
                speedometer.setProgram(umleiten);
            }
        }
   }// Settings

   Info
   {
          id: info
          width: Math.min(appwindow.width, appwindow.height)-4;
          height: width
          radius: width/2
          x: appwindow.width/2-width/2
          y: appwindow.height/2-height/2
          color: "white"
          visible: false

          onClicked:
          {
              leftelement.elementsLeftVisible();
              rightelement.elementsRightVisible();
          }

          onInfoOff:
          {
              leftelement.elementsLeftVisible();
              rightelement.elementsRightVisible();
          }
   }

   Image
   {
        id: img
        width: Math.min(appwindow.width, appwindow.height);
        height: width
        x: appwindow.width/2-width/2
        y: appwindow.height/2-height/2
        source: "/pics/Tacho.png"
        scale: width/width*1.08
        fillMode: Image.PreserveAspectFit
        visible: false
   }

   Speedometer
   {
        id: speedometer
        seconds: appwindow.sekunden
        width: Math.min(appwindow.width, appwindow.height)-12;
        height: width
        x: appwindow.width/2-width/2
        y: appwindow.height/2-height/2
        visible: false

        onStartMenuOn:
        {
            appwindow.sekunden = umleiten;
            speedometer.setProgram(appwindow.sekunden);
        }

        onShowMenu:
        {
            img.visible = false;
            speedometer.visible = false;
        }
   }
}// ApplicationWindow
