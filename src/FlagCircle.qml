﻿import QtQuick 2.0

Item
{
    id: flagcircle    
    property real angle: 0
    property int d: 0
    property alias mouseX: mouseArea.mouseX
    property alias mouseY: mouseArea.mouseY
    property string image: ""
    property real skala: 1.0
    width: d
    height: d
    antialiasing: true

    signal clicked()

    property bool containsMouse:
    {
        var x1 = width / 2;
        var y1 = height / 2;
        var x2 = mouseX;
        var y2 = mouseY;
        var diff_center = Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2);
        var radquadr = Math.pow(Math.min(width, height) / 2, 2);
        var imkreis = diff_center < radquadr;
        return imkreis;
    }

    Image
    {
        id: img
        anchors.fill: flagbox
        source: flagcircle.image
        scale: flagcircle.skala
    }

    Rectangle
    {
        id: flagbox
        border.color: "black"
        border.width: 1
        width: flagcircle.width
        height: width
        radius: width/2
        anchors.fill: flagcircle
        color: "transparent"
    }    

    MouseArea
    {
        id: mouseArea
        anchors.fill: flagcircle

        onClicked:
        {
            if(flagcircle.containsMouse)
            {               
                flagcircle.clicked();
            }
        }
    }

    function quadrantPositionX(angle)
    {
        var x, radiant;
        radiant = angle*Math.PI/180;

         if(angle<=90 && angle>0)
         {
            x = Math.round(flagbox.width * Math.cos(radiant) - flagbox.radius);
         }
         else if(angle<=0 && angle>-90)
         {
            x = Math.round(flagbox.width*Math.cos(radiant) - flagbox.radius);
         }
         else if(angle<=-90 && angle>-180)
         {
            x = Math.round(Math.abs(flagbox.width*Math.cos(radiant) - flagbox.radius));
         }
         else if(angle<=-180 && angle>-270)
         {
            x = Math.round(Math.abs(flagbox.width*Math.cos(radiant)) + flagbox.radius);
         }
         return x;
    }// quadrantPositionX

    function quadrantPositionY(angle)
    {
        var y, radiant;
        radiant = angle*Math.PI/180;

         if(angle<=90 && angle>0)
         {
             y = Math.round(flagbox.width*Math.sin(radiant) + flagbox.radius);
         }
         else if(angle<=0 && angle>-90)
         {
            y = Math.round(Math.abs(Math.abs(flagbox.width*Math.sin(radiant)) - flagbox.radius));
         }
         else if(angle<=-90 && angle>-180)
         {
            y = Math.round(Math.abs(Math.abs(flagbox.width*Math.sin(radiant)) - flagbox.radius));
         }
         else if(angle<=-180 && angle>-270)
         {
            y = Math.round(flagbox.width*Math.sin(radiant) + flagbox.radius);
         }
         return y;
    }// quadrantPositionY
}// Item
