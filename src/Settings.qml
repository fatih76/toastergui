﻿import QtQuick 2.4

Rectangle // Rectangle grauer Aussenring Heizstufen
{
   id: greyring
   property int anzahl: 5
   color: "lightgrey"
   antialiasing: true
   visible: false

   signal levelIsSet(int umleiten)
   signal settingsOff()

   FlagCircle
   {
        id: backimg
        d: Math.floor(greyring.width/6.2);
        anchors.horizontalCenter: greyring.horizontalCenter
        y: 0
        image: "/pics/zurueck.png"
        skala: 1.23
        visible: false

        onClicked:
        {
            greyring.settingsOff();
            greyring.hide();
        }
   }// FlagCircle

   Heizlevel
   {
        id: active_flag
        d: Math.floor(greyring.width/3);
        x: greyring.width/2-d/2
        y: greyring.height/2-d/2
        skala: 1
        size: Math.round(Math.min(greyring.width, greyring.height)/5);
        txt: 10
   }// Heizlevel

   Heizlevel
   {
        id: eins
        d: Math.floor(greyring.width/3);
        angle: 90-(360/greyring.anzahl/2)
        x: greyring.width/2+eins.quadrantPositionX(eins.angle);
        y: greyring.height/2-eins.quadrantPositionY(eins.angle);
        skala: 1
        size: Math.round(Math.min(greyring.width, greyring.height)/5);
        txt: 1

        onClicked:
        {
            active_flag.txt = senden;
            backimg.visible = true;
            greyring.levelIsSet(senden);
        }
   }// Heizlevel

   Heizlevel
   {
        id: zwei
        d: Math.floor(greyring.width/3);
        angle: eins.angle - (360/greyring.anzahl)
        x: greyring.width/2+zwei.quadrantPositionX(zwei.angle);
        y: greyring.height/2-zwei.quadrantPositionY(zwei.angle);
        skala: 1
        size: Math.round(Math.min(greyring.width, greyring.height)/5);
        txt: 2

        onClicked:
        {
            active_flag.txt = senden;
            backimg.visible = true;
            greyring.levelIsSet(senden);
        }
   }// Heizlevel

   Heizlevel
   {
        id: drei
        d: Math.floor(greyring.width/3);
        angle: zwei.angle - 360/greyring.anzahl
        x: greyring.width/2-drei.quadrantPositionX(drei.angle);
        y: greyring.height/2+drei.quadrantPositionY(drei.angle);
        skala: 1
        size: Math.round(Math.min(greyring.width, greyring.height)/5);
        txt: 3

        onClicked:
        {
            active_flag.txt = senden;
            backimg.visible = true;
            greyring.levelIsSet(senden);
        }
   }// Heizlevel

   Heizlevel
   {
        id: vier
        d: Math.floor(greyring.width/3);
        angle: drei.angle - 360/greyring.anzahl
        x: greyring.width/2-vier.quadrantPositionX(vier.angle);
        y: greyring.height/2-vier.quadrantPositionY(vier.angle);
        skala: 1
        size: Math.round(Math.min(greyring.width, greyring.height)/5);
        txt: 4

        onClicked:
        {
            active_flag.txt = senden;
            backimg.visible = true;
            greyring.levelIsSet(senden);
        }
   }// Heizlevel

   Heizlevel
   {
        id: fuenf
        d: Math.floor(greyring.width/3);
        angle: vier.angle - 360/greyring.anzahl
        x: greyring.width/2-fuenf.quadrantPositionX(fuenf.angle);
        y: greyring.height/2-fuenf.quadrantPositionY(fuenf.angle);
        skala: 1
        size: Math.round(Math.min(greyring.width, greyring.height)/5);
        txt: 5

        onClicked:
        {
            active_flag.txt = senden;
            backimg.visible = true;
            greyring.levelIsSet(senden);
        }
   }// Heizlevel

   Text
   {
       id: sekunden
       y: greyring.height*0.57
       anchors.horizontalCenter: greyring.horizontalCenter
       font.bold: true
       font.pixelSize: Math.round(Math.min(greyring.width, greyring.height)/19);
       color: "black"
       text:"seconds\n"
   }

   function show()
   {
        greyring.visible = true;
   }

   function hide()
   {
        greyring.visible = false;
        backimg.visible = false;
   }
}// Rectangle grauer Aussenring Heizstufen
