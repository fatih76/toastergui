﻿import QtQuick 2.4

Rectangle // Rectangle grauer Aussenring Flaggen
{
   id: greyring
   color: "lightgrey"
   visible: false
   antialiasing: true
   property int anzahl: 5
   property string language: "/text/Englisch.txt"

   signal languageOff(string lng)

   FlagCircle
   {
        id: backbox
        d: Math.floor(greyring.width/6.2);
        anchors.horizontalCenter: greyring.horizontalCenter
        y: 0
        image: "/pics/zurueck.png"
        skala: 1.23
        visible: false

        onClicked:
        {
            greyring.languageOff(greyring.language);
            greyring.hide();
        }
   }// FlagCircle

   FlagCircle
   {
        id: active_flag
        d: Math.floor(greyring.width/3);
        x: greyring.width/2-d/2
        y: greyring.height/2-d/2
        image: "/pics/England.png"
        skala: 1.027
   }// FlagCircle

   FlagCircle
   {
        id: italy
        d: Math.floor(greyring.width/3);
        angle: 90-(360/greyring.anzahl/2)
        x: greyring.width/2+italy.quadrantPositionX(italy.angle);
        y: greyring.height/2-italy.quadrantPositionY(italy.angle);
        image: "/pics/Italy.png"
        skala: 1.01

        onClicked:
        {
            active_flag.image = italy.image;
            greyring.language = "/text/Italienisch.txt";
            backbox.visible = true;
        }       
   }// FlagCircle

   FlagCircle
   {
        id: france
        d: Math.floor(greyring.width/3);
        angle: italy.angle-(360/greyring.anzahl);
        x: greyring.width/2+france.quadrantPositionX(france.angle);
        y: greyring.height/2-france.quadrantPositionY(france.angle);
        image: "/pics/French.png"
        skala: 1.03

        onClicked:
        {
            active_flag.image = france.image;
            greyring.language = "/text/Franzoesisch.txt";
            backbox.visible = true;
        }
   }// FlagCircle

   FlagCircle
   {
        id: germany
        d: Math.floor(greyring.width/3);
        angle: france.angle-360/greyring.anzahl;
        x: greyring.width/2-germany.quadrantPositionX(germany.angle);
        y: greyring.height/2+germany.quadrantPositionY(germany.angle);
        image: "/pics/Germany.png"
        skala: 1.015

        onClicked:
        {
            active_flag.image = germany.image;
            greyring.language = "/text/Deutsch.txt";
            backbox.visible = true;
        }
   }// FlagCircle

   FlagCircle
   {
        id: england
        d: Math.floor(greyring.width/3);
        angle: germany.angle-360/greyring.anzahl;
        x: greyring.width/2-england.quadrantPositionX(england.angle);
        y: greyring.height/2-england.quadrantPositionY(england.angle);
        image: "/pics/England.png"
        skala: 1.02

        onClicked:
        {
            active_flag.image = england.image;
            greyring.language = "/text/Englisch.txt";
            backbox.visible = true;
        }
   }// FlagCircle

   FlagCircle
   {
        id: china
        d: Math.floor(greyring.width/3);
        angle: england.angle-360/greyring.anzahl;
        x: greyring.width/2-china.quadrantPositionX(china.angle);
        y: greyring.height/2-china.quadrantPositionY(china.angle);
        image: "/pics/China.png"
        skala: 1.02

        onClicked:
        {
            active_flag.image = china.image;
            greyring.language = "/text/Chinesisch.txt";
            backbox.visible = true;
        }
   }// FlagCircle

   function show()
   {
        greyring.visible = true;
   }

   function hide()
   {
        greyring.visible = false;
        backbox.visible = false;
   }
}// Rectangle grauer Aussenring Flaggen



