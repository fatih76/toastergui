﻿import QtQuick 2.0

Canvas
{
    id: linksunten
    antialiasing: true
    property int w:0
    property int h:0
    property int xpos: 0
    property int ypos: 0
    width: w
    height: h
    x: xpos
    y: ypos

    signal languageOn()
    signal startToaster()

    FlagCircle
    {
         id: playbtn
         width: 0.468085*linksunten.width
         height: 0.468085*linksunten.height
         x: 0.12766*Math.round(linksunten.width);
         y: 0.0212766*Math.round(linksunten.height);
         image: "/pics/play.png"
         skala: 1.2
         visible: false

         onClicked:
         {
             linksunten.startToaster();
         }
    }// FlagCircle

    FlagCircle
    {
         id: lngbtn
         width: 0.468085*linksunten.width
         height: 0.468085*linksunten.height
         x: linksunten.width-width-(0.0212766*Math.round(linksunten.width));
         y: linksunten.height-height-(0.12766*Math.round(linksunten.height));
         image: "/pics/sprachen.png"
         skala: 1.0275
         visible: false

         onClicked:
         {
             linksunten.languageOn();
         }
    }// FlagCircle

    function elementsLeftInvisible()
    {
        playbtn.visible = false;
        lngbtn.visible = false;
    }

    function elementsLeftVisible()
    {
        playbtn.visible = true;
        lngbtn.visible = true;
    }

    onPaint:
    {
        var ctxleft = linksunten.getContext("2d");
        ctxleft.globalAlpha = 0.5;
        ctxleft.save();
        var gradient = ctxleft.createRadialGradient(linksunten.width, 0, 0, linksunten.width, 0, linksunten.width);
        gradient.addColorStop(1, "orange");
        ctxleft.clearRect(0, 0, linksunten.width, linksunten.height);
        ctxleft.beginPath();
        ctxleft.lineWidth = linksunten.width;
        ctxleft.strokeStyle = gradient
        ctxleft.arc(linksunten.width, 0, linksunten.width-ctxleft.lineWidth/2, Math.PI/2, Math.PI, false);
        ctxleft.stroke();
        ctxleft.restore();
    }
}// Canvas
