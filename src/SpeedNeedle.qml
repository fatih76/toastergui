﻿import QtQuick 2.4
import QtGraphicalEffects 1.0 // => DropShadow

Canvas
{
    id: canvas
    antialiasing: true
    property real centerWidth: width / 2
    property real centerHeight: centerWidth
    property real radius: width/2
    property real minimumValue: -360
    property real maximumValue: 0
    property real currentValue: -360
    property real angle: (currentValue-minimumValue)/(maximumValue-minimumValue)*2*Math.PI+0.01;
    property real angleOffset: 20.955
    property int program: 10
    property int cpy_program: program
    property int value : 0

    signal startMenuOn(int senden)
    signal showMenu()

    onValueChanged:
    {
        canvas.currentValue = zeiger.rotation - 211;
    }

    onProgramChanged:
    {
        canvas.minimumValue = -360;
        canvas.maximumValue = 0;
        canvas.currentValue = -360;
        canvas.value = 100;
        remains.cnt = canvas.program;
        remains.text = remains.cnt.toFixed(1).toString();
        zeiger.rotation = -149;
        canvas.requestPaint();
    }

    Rectangle // Rectangle-Zeiger
    {
        id: zeiger
        rotation: -149
        width: 2
        height: parent.width / 2
        transformOrigin: Item.Bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.verticalCenter
        smooth: true
        antialiasing: true
        color: "red"
        state: "back"

        onRotationChanged:
        {
            canvas.currentValue = zeiger.rotation - 211;
            canvas.requestPaint();
        }

        states:
        [
            State
            {
                name: "rotated"

                PropertyChanges
                {
                    target: zeiger
                    rotation: 149
                }
            },

            State
            {
                name: "back"

                PropertyChanges
                {
                    target: zeiger
                    rotation: -149
                }
            }
        ] // states

        transitions:
        [
            Transition
            {
                from: "back"
                to: "rotated"

                RotationAnimation
                {
                  duration: canvas.program * 1000
                  direction: RotationAnimation.Clockwise
                }
            },

            Transition
            {
                from: "rotated"
                to: "back"

                RotationAnimation
                {
                  duration: 0
                  direction: RotationAnimation.Clockwise
                }
            }
        ]// transitions
    }// Rectangle-Zeiger

    Timer
    {
        id: timer_countdown
        interval: 100
        running:  false
        repeat:   true

        onTriggered:
        {
            if(remains.cnt > 0.0)
            {
                remains.cnt = remains.cnt.toFixed(1);
                remains.cnt -= 0.1;
                remains.text = remains.cnt.toFixed(1).toString();
            }
            else
            {
                timer_countdown.stop();
                zeiger.state = "back";
                remains.cnt = 0.0;
                remains.text = remains.cnt.toFixed(1).toString();

                for(var i=0 ; i.toFixed(1)<100000 ; i+=0.1){}

                remains.cnt = canvas.cpy_program;
                remains.text = remains.cnt.toFixed(1).toString();
                balken.state = "";
                balken.width = 0;
                balken.color = "";

                for(var k=0 ; k.toFixed(1)<100000 ; k+=0.1){}

                canvas.showMenu();
            }
        }
    }// Timer_Countdown

    Timer
    {
        id: timer_percentage
        interval: 10*canvas.program
        running:  false
        repeat:   true

        onTriggered:
        {
            if(fortschritt.progress < 100)
            {
                fortschritt.progress += 1;
                fortschritt.text = fortschritt.progress.toString()+"%";
            }
            else
            {
                timer_percentage.stop();
                fortschritt.progress = 0;
                fortschritt.text = fortschritt.progress.toString()+"%";
            }
        }
    }// Timer_Percentage

    Image
    {
         id: innerring         
         width: canvas.width*0.5
         height: width
         x: canvas.width/2-width/2
         y: canvas.height/2-height/2
         source: "/pics/Tacho_Mitte.png"
         scale: width/width*1.16

         Text
         {
             id: remains
             anchors.horizontalCenter: innerring.horizontalCenter
             y: Math.round(canvas.height/13);
             property real cnt: canvas.program.toFixed(1);
             text: cnt.toFixed(1).toString();
             font.pixelSize: Math.round(innerring.width/5);
             font.bold: true
             font.family: "Eurostile"
             color: "white"
         }

         DropShadow
         {
                 id: dropshadow
                 anchors.fill: remains
                 horizontalOffset: 0
                 verticalOffset: 8
                 radius: 4.0
                 samples: 16
                 color: "black"
                 source: remains
         }

         Text
         {
             id: textsec
             text: "<i>sekunden</i>"
             font.pixelSize: Math.round(innerring.width/8);
             font.bold: true
             font.family: "Eurostile"
             y: Math.round(canvas.height/5.5);
             color: "white"
             anchors.horizontalCenter: innerring.horizontalCenter
         }                  

         Text
         {
             id: fortschritt
             property int progress: 0
             text: fortschritt.progress.toString()+"%";
             font.pixelSize: Math.round(innerring.width/7);
             font.bold: true
             font.family: "Eurostile"
             anchors.horizontalCenter: innerring.horizontalCenter
             x: Math.round(innerring.width*0.27);
             y: Math.round(canvas.height/3);
             color: "white"
         }
    }// Image

    Rectangle // Fortschrittsbalken GRÜN, ORANGE, ROT
    {
       id: balken
       width: 0
       height: canvas.height/12
       x: canvas.width/2-rahmen.width/2
       y: Math.round(canvas.height/2);
       color: "green"
       state: ""

       states:
       [
           State
           {
               name: "green"

               PropertyChanges
               {
                   target: balken
                   width: Math.round(Math.min(canvas.width, canvas.height)*0.4/5*1);
                   color: "green"
               }
           },

           State
           {
               name: "lightgreen"

               PropertyChanges
               {
                   target: balken
                   width: Math.round(Math.min(canvas.width, canvas.height)*0.4/5*2);
                   color: "lightgreen"
               }
           },

           State
           {
               name: "yellow"

               PropertyChanges
               {
                   target: balken
                   width: Math.round(Math.min(canvas.width, canvas.height)*0.4/5*3);
                   color: "yellow"
               }
           },

           State
           {
               name: "orange"

               PropertyChanges
               {
                   target: balken
                   width: Math.round(Math.min(canvas.width, canvas.height)*0.4/5*4);
                   color: "orange"
               }
           },

           State
           {
               name: "red"

               PropertyChanges
               {
                   target: balken
                   width: Math.round(Math.min(canvas.width, canvas.height)*0.4/5*5);
                   color: "red"
               }
           }
       ]// states

       transitions:
       [
               Transition
               {
                   to: "green"

                   NumberAnimation
                   {
                       properties: "width"
                       duration: canvas.program * 1000
                   }
               },

               Transition
               {
                   to: "lightgreen"

                   NumberAnimation
                   {
                       properties: "width"
                       duration: canvas.program * 1000
                   }
               },

               Transition
               {
                   to: "yellow"

                   NumberAnimation
                   {
                       properties: "width"
                       duration: canvas.program * 1000
                   }
               },

               Transition
               {
                   to: "orange"

                   NumberAnimation
                   {
                       properties: "width"
                       duration: canvas.program * 1000
                   }
               },

               Transition
               {
                   to: "red"

                   NumberAnimation
                   {
                       properties: "width"
                       duration: canvas.program * 1000
                   }
               }
       ]// transitions
    }// Rectangle Fortschrittsbalken GRÜN, GELB, ORANGE, ROT

    Rectangle
    {
        id: rahmen
        width: canvas.width*0.4
        height: canvas.height/12
        x: canvas.width/2-width/2
        y: Math.round(canvas.height/2);
        color: "transparent"
        border.width: Math.min(canvas.width, canvas.height)/Math.min(canvas.width, canvas.height);
        border.color: "grey"
    }

    Rectangle // Rectangle ABORT-Button
    {
        id: abort
        width: canvas.width/6
        height: width
        radius: width/2
        color:"yellow"
        x: canvas.width/2-width/2
        y: canvas.height-height

        Text
        {
            id: txt
            anchors.verticalCenter: abort.verticalCenter
            anchors.horizontalCenter: abort.horizontalCenter
            text: "ABORT"
            font.bold: true
            font.pixelSize: Math.round(innerring.width/12);
            color:"black"
        }

        MouseArea
        {
            id: mouseaction
            anchors.fill: abort

            onClicked:
            {
                timer_countdown.stop();
                timer_percentage.stop();
                zeiger.state = "back";
                remains.cnt = canvas.cpy_program;
                remains.text = remains.cnt.toFixed(1).toString();
                fortschritt.progress = 0;
                fortschritt.text = fortschritt.progress.toString();
                balken.state = "";
                balken.width = 0;
                balken.color = "";
                canvas.showMenu();
            }
        }
    }// Rectangle ABORT-Button

    onPaint:
    {
          var ctx = getContext("2d");
          ctx.save();
          var gradient2 = ctx.createRadialGradient((parent.width / 2),(parent.height / 2), 0, (parent.width / 2),(parent.height / 2),parent.height);
          gradient2.addColorStop(0.5, "#81FFFE");
          gradient2.addColorStop(0.46, "#81FFFE");
          gradient2.addColorStop(0.45, "#112478");
          gradient2.addColorStop(0.33, "transparent");
          ctx.clearRect(0, 0, canvas.width, canvas.height);
          ctx.beginPath();
          ctx.lineWidth = 150;
          ctx.strokeStyle = gradient2
          ctx.arc(canvas.centerWidth, canvas.centerHeight, canvas.radius-ctx.lineWidth/2, canvas.angleOffset, canvas.angleOffset+canvas.angle);
          ctx.stroke();
          ctx.restore();
    }// onPaint:

    function setProgram(sec)
    {
        canvas.program = sec;
        canvas.cpy_program = sec;
    }

    function startUnfreeze()
    {
        if(canvas.program>=10 && canvas.program<=50)
        {
            zeiger.state = "rotated";
            timer_countdown.start();
            timer_percentage.start();

            switch(canvas.program)
            {
                case 50:    balken.state = "green"; break;
                case 40:    balken.state = "lightgreen"; break;
                case 30:    balken.state = "yellow"; break;
                case 20:    balken.state = "orange"; break;
                case 10:    balken.state = "red"; break;
            }
        }
    }//startUnfreeze
}// Canvas
