﻿import QtQuick 2.4

Item
{
    id: speedometer
    property int seconds: 10
    antialiasing: true

    signal startMenuOn(int umleiten)
    signal showMenu()

    onSecondsChanged:
    {
        return speedometer.seconds;
    }

    SpeedNeedle
    {
       id: speedneedle
       width: speedometer.width
       height: width
       x: speedometer.width/2-width/2
       y: speedometer.height/2-height/2
       program: speedometer.seconds
       focus: true

       onStartMenuOn:
       {
            speedometer.startMenuOn(senden);
       }

       onShowMenu:
       {
            speedometer.showMenu();
       }
    }// SpeedNeedle

    function setProgram(sec)
    {
        speedometer.seconds = sec;
        speedneedle.setProgram(sec);
    }

    function startUnfreeze()
    {
        speedneedle.startUnfreeze();
    }
 }// Item
