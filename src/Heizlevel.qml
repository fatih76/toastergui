﻿import QtQuick 2.0

Item
{
    id: heizlevel
    property real angle: 0
    property int d: 0
    property string image: ""
    width: d
    height: d
    antialiasing: true
    property alias mouseX: mouseArea.mouseX
    property alias mouseY: mouseArea.mouseY
    property real skala: 1.0
    property int txt: 10
    property int size: 30

    signal clicked(int senden)

    property bool containsMouse:
    {
        var x1 = width / 2;
        var y1 = height / 2;
        var x2 = mouseX;
        var y2 = mouseY;
        var diff_center = Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2);
        var radquadr = Math.pow(Math.min(width, height) / 2, 2);
        var imkreis = diff_center < radquadr;
        return imkreis;
    }

    Image
    {
        id: img
        anchors.fill: levelbox
        source: heizlevel.image
        scale: heizlevel.skala
    }

    Rectangle
    {
        id: levelbox
        width: heizlevel.d
        height: width
        radius: width/2
        border.color: "black"
        color: "orange"
        border.width: 1
        anchors.fill: heizlevel

        Text
        {
             id: heizzeit
             anchors.fill: levelbox
             verticalAlignment: Text.AlignVCenter
             horizontalAlignment: Text.AlignHCenter
             font.pixelSize: heizlevel.size
             font.bold: true
             text: heizlevel.txt.toString();
        }// Text
    }// Rectangle    

    MouseArea
    {
        id: mouseArea
        anchors.fill: levelbox

        onClicked:
        {
            if(heizlevel.containsMouse) // Aufruf containsMouse
            {
                var wert = heizlevel.getSekunden(heizlevel.txt);
                heizlevel.clicked(wert);
            }
        }
    }

    function quadrantPositionX(angle)
    {
        var x, radiant;
        radiant = angle*Math.PI/180;

         if(angle<=90 && angle>0)
         {
            x = Math.round(levelbox.width * Math.cos(radiant) - levelbox.radius);
         }
         else if(angle<=0 && angle>-90)
         {
            x = Math.round(levelbox.width*Math.cos(radiant) - levelbox.radius);
         }
         else if(angle<=-90 && angle>-180)
         {
            x = Math.round(Math.abs(levelbox.width*Math.cos(radiant) - levelbox.radius));
         }
         else if(angle<=-180 && angle>-270)
         {
            x = Math.round(Math.abs(levelbox.width*Math.cos(radiant)) + levelbox.radius);
         }
         return x;
    }// quadrantPositionX

    function quadrantPositionY(angle)
    {
        var y, radiant;
        radiant = angle*Math.PI/180;

         if(angle<=90 && angle>0)
         {
             y = Math.round(levelbox.width*Math.sin(radiant) + levelbox.radius);
         }
         else if(angle<=0 && angle>-90)
         {
            y = Math.round(Math.abs(Math.abs(levelbox.width*Math.sin(radiant)) - levelbox.radius));
         }
         else if(angle<=-90 && angle>-180)
         {
            y = Math.round(Math.abs(Math.abs(levelbox.width*Math.sin(radiant)) - levelbox.radius));
         }
         else if(angle<=-180 && angle>-270)
         {
            y = Math.round(levelbox.width*Math.sin(radiant) + levelbox.radius);
         }
         return y;
    }// quadrantPositionY

    function getSekunden(level)
    {
        return 60-10*level;
    }

    function getTxt()
    {
        return heizzeit.text.valueOf();
    }
}// Item
