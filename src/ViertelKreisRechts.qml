﻿import QtQuick 2.0

Canvas
{
    id: rechtsunten
    antialiasing: true
    property int w:0
    property int h:0
    property int xpos: 0
    property int ypos: 0
    property string sprache: "/text/Englisch.txt"
    width: w
    height: h
    x: xpos
    y: ypos

    signal infoOn()
    signal settingsOn()

    FlagCircle
    {
         id: settingsbtn
         width: 0.468085*rechtsunten.width
         height: 0.468085*rechtsunten.height
         x: 0.0212766*Math.round(rechtsunten.width);
         y: rechtsunten.height-height-(0.12766*Math.round(rechtsunten.height));
         image: "/pics/settings.png"
         skala: 1.02
         visible: false

         onClicked:
         {
             rechtsunten.settingsOn();
         }
    }// FlagCircle

    FlagCircle
    {
         id: infobtn
         width: 0.468085*rechtsunten.width
         height: 0.468085*rechtsunten.height
         x: rechtsunten.width-width-(0.12766*Math.round(rechtsunten.width))
         y: 0.0212766*Math.round(rechtsunten.height);
         image: "/pics/info.png"
         skala: 1.18
         visible: false

         onClicked:
         {
            rechtsunten.infoOn();
         }
    }// FlagCircle

    function elementsRightInvisible()
    {
        settingsbtn.visible = false;
        infobtn.visible = false;
    }

    function elementsRightVisible()
    {
        settingsbtn.visible = true;
        infobtn.visible = true;
    }

    onPaint:
    {
        var ctxright = rechtsunten.getContext("2d");
        ctxright.globalAlpha = 0.5;
        ctxright.save();
        var gradient = ctxright.createRadialGradient(0, 0, 0, 0, 0, rechtsunten.width);
        gradient.addColorStop(1, "lightgreen");
        ctxright.clearRect(0, 0, rechtsunten.width, rechtsunten.height);
        ctxright.beginPath();
        ctxright.lineWidth = rechtsunten.width;
        ctxright.strokeStyle = gradient
        ctxright.arc(0, 0, rechtsunten.width-ctxright.lineWidth/2, 0, Math.PI/2, false);
        ctxright.stroke();
        ctxright.restore();
    }
}// Canvas
