﻿import QtQuick 2.7
import QtTest 1.0

Item
{
    width: 800
    height: 600

    ExpandButton
    {
        id: expandButton
        anchors.centerIn: parent
    }

    TestCase
    {
        name: "ExpandButton"
        when: windowShown

        function test_clickToExpand()
        {
            var widthBeforeClick = expandButton.width;
            mouseClick(expandButton);
            var widthAfterClick = expandButton.width;
            verify(widthBeforeClick < widthAfterClick);
        }
    }
}
