﻿import QtQuick 2.11
import QtQuick.Controls 2.4 // => ScrollBar

Rectangle //Rectangle textoutput
{
    id: textoutput
    property string img: ""
    visible: false
    antialiasing: true

    signal clicked()
    signal infoOff()

    Rectangle
    {
        id: circlerect
        width: textoutput.width
        height: width
        x: textoutput.width/2-width/2
        y: textoutput.height/2-height/2
        color: "transparent"
        radius: width/2

        Rectangle
        {
            id: frame
            clip: true
            width: 2*(circlerect.width/2)*Math.cos(30*Math.PI/180)
            height: 2*(circlerect.width/2)*Math.sin(30*Math.PI/180)
            color: "white"
            anchors.centerIn: parent

            Text
            {
                id: content
                font.pixelSize: Math.round(Math.min(textoutput.width, textoutput.height)/17);
                font.bold: true

                MouseArea
                {
                    id: mouseArea
                    anchors.fill: content
                    drag.target: content
                    drag.minimumX: 0
                    drag.minimumY: frame.height-content.height
                    drag.maximumX: 0
                    drag.maximumY: 0
                }//MouseArea
            }//Text

            ScrollBar
            {
                id: verticalIndicator
                active: mouseArea.pressed
                orientation: Qt.Vertical
                size: frame.height / content.height
                position: -content.y / content.height

                anchors
                {
                    top: parent.top
                    right: parent.right
                    bottom: parent.bottom
                }
            }//ScrollBar
        }//Rectangle frame
    }//Rectangle circlerect

    FlagCircle
    {
         id: backbox
         d: Math.floor(textoutput.width/6.2);
         anchors.horizontalCenter: textoutput.horizontalCenter
         y: textoutput.height-d
         image: "/pics/zurueck.png"
         skala: 1.23

         onClicked:
         {
             textoutput.infoOff();
             textoutput.hide();
             content.y = 0;
         }//onClicked
    }// FlagCircle

    function show()
    {
         textoutput.visible = true;
    }

    function hide()
    {
         textoutput.visible = false;
    }

    function readTextFile(fileUrl)
    {
       var xhr = new XMLHttpRequest;
       xhr.open("GET", fileUrl);
       xhr.onreadystatechange =    function ()
                                   {
                                       if(xhr.readyState === XMLHttpRequest.DONE)
                                       {
                                           var response = xhr.responseText;
                                           response = response.replace(/[�]/g, 'é');
                                           response = fitInRectangle(response);
                                           content.text = response;
                                       }
                                   }
       xhr.send();
    }// readTextFile

    function fitInRectangle(inhalt)
    {
        var line = "";
        var alltext = "";

        for(var i=0, n=1; i<inhalt.length ; i++)
        {
            if(n<20)
            {
                if(inhalt[i] !== '\n' && inhalt[i] !== '')
                {
                    line += inhalt[i];
                    n++;
                }

                if(inhalt[i] === '')
                {
                    alltext += line;
                    line = "";
                    n=1;
                }

                if(inhalt[i] === '\n')
                {
                    line += inhalt[i]+'\n';
                    alltext += line;
                    line = "";
                    n=1;
                }
            }//if
            else if(n>=20)
            {
                if(inhalt[i] !== '\n' && inhalt[i] !== '' && inhalt[i] !== ' ')
                {                   
                    line += inhalt[i];
                    n++;
                }

                if(inhalt[i] === ' ')
                {
                    line += '\n';
                    alltext += line;
                    line = "";
                    n=1;
                }

                if(inhalt[i] === '\n')
                {
                    line += inhalt[i]+'\n';
                    alltext += line;
                    line = "";
                    n=1;
                }

                if(inhalt[i] === '')
                {
                    alltext += line;
                    line = "";
                    n=1;
                }
            }//else
        }//for

        return alltext;
    }// fitInRectangle
}//Rectangle textoutput
